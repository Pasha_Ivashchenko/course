
window.addEventListener('load',function () {

    var topicContainer = document.getElementById('topicContainer');
    var topicForm = document.getElementById('topicForm');
    var topicFormAuthor = topicForm.author;
    var topicFormText = topicForm.textBlock;
    var topicFormInputs = [topicFormAuthor,topicFormText];
    var topicTemplate = document.getElementById('topicTemplate');
    var formErrContainer = document.getElementById('formErrContainer');
    var wrap = document.getElementById('wrap');
    var closeFormError = document.getElementById('closeFormError');
    var addTopicBtn = document.getElementById('addTopicBtn');
    var closeTopicFormBtn = document.getElementById('closeTopicFormBtn');
    var preview = document.getElementById('preview');
    var imageLink = document.getElementById('imageLink');
    var submitTopicBtn = document.getElementById('submitTopicBtn');
    var topicCount = 0;

    if(topicContainer.children.length < 1) {
        topicForm.classList.remove('invisible');
    };

    function addTopic(event) {
        event.preventDefault();
        var res = validateForm(topicFormInputs);
        if(res > 0){
            var errorsHeight = (res * 18) + 'px';
            formErrContainer.style.height = errorsHeight;
            formErrContainer.style.padding = '5px 0';
            closeTopicFormBtn.classList.contains('invisible') ? null :
                closeTopicFormBtn.classList.add('invisible');
            return;
        };

        clearFormErrs();
        gototop();

        closeTopicFormBtn.classList.remove('invisible');
        topicCount++;
        var newTopic = topicTemplate.cloneNode(true);
        newTopic.setAttribute('id', 'topic_'+topicCount);
        var newTopicFields = newTopic.children;
        var topicImg = newTopic.getElementsByTagName('IMG')[0];
        //populate topic fields
        newTopicFields[0].innerHTML = topicFormAuthor.value;
        newTopicFields[1].innerHTML = getDate(false);
        newTopicFields[2].innerHTML = topicFormText.value;
        if(imageLink.value.length > 4){
            topicImg.src = imageLink.value;
            topicImg.classList.remove('invisible');
        };
        newTopic.getElementsByClassName('writeComment')[0].addEventListener('click', function (event) {
            writeComment(event);
        });
        newTopic.getElementsByClassName('commentsForm')[0].addEventListener('submit', function (event) {
            leaveComment(event);
        });
        newTopic.getElementsByClassName('lowerBtns')[1].addEventListener('click', function (event) {
            showComments(event);
        });
        newTopic.getElementsByClassName('likes')[0].addEventListener('click', function (event) {
            thumbsUp(event);
        });
        //style all things
        addTopicBtn.classList.remove('invisible');
        topicContainer.children.length > 0 ? topicContainer.insertBefore(newTopic, topicContainer.children[0]) :
            topicContainer.appendChild(newTopic);
        newTopic.classList.remove('invisible');
        if(topicContainer.children.length > 1){
            document.getElementsByClassName('lowerBtns')[0].style.opacity = 1;
            document.getElementsByClassName('lowerBtns')[1].style.opacity = 1;
        } else {
            setTimeout(function () {
                document.getElementsByClassName('lowerBtns')[0].style.opacity = 1;
                document.getElementsByClassName('lowerBtns')[1].style.opacity = 1;
            }, 1500);
        }
        topicContainer.style.width = '50%';
        topicForm.style.width = '50%';
        //reset form fields
        topicFormAuthor.value = '';
        topicFormText.value = '';
        imageLink.value = '';
    };

    function validateForm (inputs) {
        var errors = 0;
        [].forEach.call(inputs, function (elem) {
            if(elem.value.length === 0){
                var error = document.createElement('div');
                error.name = elem.name;
                error.className = 'formError';
                error.innerHTML = 'Please fill ' + elem.name + ' field!';
                elem.className = 'inputError';
                formErrContainer.appendChild(error);
                errors++;
            };
        });
        return errors;
    };

    function clearFormErrs () {
        formErrContainer.style.padding = '0px';
        formErrContainer.style.height = 0;
        setTimeout(function () {
            for(var i = 0; i < formErrContainer.childElementCount - 1; i++){
                if(formErrContainer.children[i].tagName === 'DIV'){
                    formErrContainer.removeChild(formErrContainer.children[i]);
                }
            }
        }, 500);
        [].forEach.call(topicFormInputs,function (elem) {
            if(elem.className === 'inputError'){
                elem.classList.remove('inputError');
            }
        });
        closeTopicFormBtn.classList.contains('invisible') && topicContainer.children.length >= 1 ?
            closeTopicFormBtn.classList.remove('invisible') : null;

    };

    function getDate(flag) {
        var newDate = new Date();
        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];
        var hours = newDate.getHours() < 10 ? '0' + newDate.getHours() : newDate.getHours();
        var minutes = newDate.getMinutes() < 10 ? '0' + newDate.getMinutes() : newDate.getMinutes();
        var day = newDate.getDate(true);
        var monthIndex = newDate.getMonth();
        var year = newDate.getFullYear();
        return flag ? hours + ':' + minutes + ' ' + day + ' ' + monthNames[monthIndex] + ' ' + year :
            day + ' ' + monthNames[monthIndex] + ' ' + year;
    }
    
    function closeTopicForm() {
        closeTopicFormBtn.classList.add('invisible');
        submitTopicBtn.style.transition = '2s';
        topicForm.style.width = '0px';
        submitTopicBtn.style.width = '0px';
        addTopicBtn.removeEventListener('click', openTopicForm);
        setTimeout(function () {
            topicForm.style.display = 'none';
            addTopicBtn.addEventListener('click', openTopicForm);
        }, 1300);
        topicContainer.style.width = '100%';
        addTopicBtn.classList.remove('invisible');
    }

    function openTopicForm() {
        document.body.style.backgroundColor = randomColor();
        gototop();
        topicForm.style.display = 'block';
        topicContainer.style.width = '50%';
        setTimeout(function () {
            topicForm.style.width = '50%';
            submitTopicBtn.style.width = '160px';
        }, 10);
        closeTopicFormBtn.classList.remove('invisible');
    }

    function addImage(event) {
        if(event.target.value.length < 5){
            return;
        };
        preview.src = event.target.value;
        preview.classList.remove('invisible');
    }

    function writeComment(event) {
        var currentTopic = event.target.parentNode.parentNode;
        var currCommentsForm = currentTopic.getElementsByClassName('commentsForm')[0];
        var allWriteCommentBtns = document.getElementsByClassName('commentsForm');
        if(currCommentsForm.style.maxHeight !== '500px'){
            [].forEach.call(allWriteCommentBtns, function (elem) {
                elem.style.transition = '0.5s';
                elem.style.maxHeight = '0';
            });
            currCommentsForm.style.transition = '2s';
            currCommentsForm.style.maxHeight = '500px';
        } else {
            currCommentsForm.style.transition = '1s'
            currCommentsForm.style.maxHeight = '0px';
        }
    }

    function leaveComment(event) {
        event.preventDefault();
        var currentTopic = event.target.parentNode;
        var currCommentsCont = currentTopic.getElementsByClassName('commentsCont')[0];
        var currCommentsForm = currentTopic.getElementsByClassName('commentsForm')[0];
        var newComment = currentTopic.getElementsByClassName('sampleComment')[0].cloneNode(true);
        //populate fields
        var commentsCount = currentTopic.getElementsByTagName('SPAN')[0];
        commentsCount.innerHTML = '(' + ((+commentsCount.innerHTML.slice(1,commentsCount.innerHTML.indexOf(')'))) + 1) + ')';
        newComment.children[0].innerHTML = currCommentsForm.children[0].value;
        newComment.children[1].innerHTML = getDate(true);
        console.log(newComment.children[1].innerHTML);
        newComment.children[2].innerHTML = currCommentsForm.children[1].value;
        currCommentsCont.appendChild(newComment);
        //style up
        newComment.classList.remove('invisible');
        currCommentsCont.style.maxHeight = '500px';
    }

    function gototop() {
        if (window.scrollY>0) {
            window.scrollTo(0,window.scrollY-20)
            setTimeout(function(){
                gototop();
            },10)
        }
    }

    function showComments(event) {
        var currentTopic = event.target.parentNode.parentNode;
        var currCommentsCont = currentTopic.getElementsByClassName('commentsCont')[0];
        if(currCommentsCont.children.length < 1)return;
        if(currCommentsCont.style.maxHeight !== '500px'){
            currCommentsCont.style.transition = '1.5s';
            currCommentsCont.style.maxHeight = '500px';
        } else {
            currCommentsCont.style.transition = '1s';
            currCommentsCont.style.maxHeight = '0px';
        }
    }

    function thumbsUp (event) {
        event.target.innerHTML = +event.target.innerHTML + 1;
    }

    function randomColor() {
        return "#" + Math.random().toString(16).slice(2, 8);
    };

    //add event handlers

    topicForm.addEventListener('submit', function (event) {
        addTopic(event);
    });

    closeFormError.addEventListener('click', clearFormErrs);

    closeTopicFormBtn.addEventListener('click', closeTopicForm);

    addTopicBtn.addEventListener('click', openTopicForm);

    imageLink.addEventListener('input', addImage);

});